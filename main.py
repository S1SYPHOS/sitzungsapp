from sitzungsapp import create_app

app = create_app()


if __name__ == "__main__":
    # Run application
    app.run(host="localhost", port=1024)
