export default () => ({
    files: [],

    hasFiles() {
        return this.files.length > 0
    },

    input: {
        ['@change']() {
            this.files = this.$event.target.files
        },
        ['@dragover']() {
            this.$root.classList.add('active')
            this.$root.classList.remove('inactive')
        },
        ['@dragleave']() {
            this.$root.classList.remove('active')
            this.$root.classList.add('inactive')
        },
        ['@drop']() {
            this.$root.classList.remove('active')
            this.$root.classList.add('inactive')
        },
    },
})
