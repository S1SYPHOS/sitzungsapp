export default () => ({
    cursorSpeed: 1000,

    init() {
        // Initialize blinking cursor
        setInterval(() => {
            if (this.$refs.cursor.classList.contains('hidden')) {
                this.$refs.cursor.classList.remove('hidden')

            } else {
                this.$refs.cursor.classList.add('hidden')
            }
        }, this.cursorSpeed);
    }
})
