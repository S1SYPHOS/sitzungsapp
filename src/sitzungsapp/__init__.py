"""
This module is part of the 'sitzungsapp' package,
which is released under GPL-3.0-only license.
"""

from fastapi import FastAPI

from .errors import http_error
from .routes.api import api
from .routes.assets import assets
from .routes.imprint import imprint
from .routes.project import project
from .templating import static


def create_app():
    """
    Createss new app instance
    """

    # Create app
    app = FastAPI(title="Sitzungsapp")

    # Add error handler
    app.add_exception_handler(*http_error)

    # Register routers
    # (1) App
    app.include_router(api)

    # (2) Cache-busting
    app.include_router(assets)

    # (3) Pages & directories
    app.include_router(imprint)
    app.include_router(project)

    # Mount static files
    app.mount(*static)

    return app
