"""
This module is part of the 'sitzungsapp' package,
which is released under GPL-3.0-only license.
"""

import pathlib

from pydantic import BaseSettings


# pylint: disable=R0903
class Config(BaseSettings):
    """
    Holds configuration
    """

    # Application directory
    app_dir: pathlib.Path = pathlib.Path(__file__).parent / "app"

    # Limit upload size to 128 KB
    max_content_length: int = 128 * 1024


config = Config()
