"""
This module is part of the 'sitzungsapp' package,
which is released under GPL-3.0-only license.
"""

import json
from typing import Any


def json2string(data: dict[str, Any] | list[str]) -> str:
    """
    Converts JSON data to indented string

    :param data: dict[str, Any] | list[str]
    :return: str
    """

    return json.dumps(data, ensure_ascii=False, indent=4)


def reverse_datetime(date: str) -> str:
    """
    Reverses datestring

    :param date: str Datestring in 'YYYY-MM-DD' format
    :return: str Formatted datestring in 'DD.MM.YYYY' format
    """

    return ".".join(reversed(date.split("-")))


j2_filters = {
    "dt": reverse_datetime,
    "json2string": json2string,
}
