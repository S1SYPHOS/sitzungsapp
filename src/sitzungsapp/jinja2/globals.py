"""
This module is part of the 'sitzungsapp' package,
which is released under GPL-3.0-only license.
"""

from pkg_resources import working_set

from ..config import config


def current_version() -> str:
    """
    Provides current version

    :return: str
    """

    # Loop over installed packages
    for package in list(working_set):
        # If current package detected ..
        if package.project_name == "sitzungsapp":
            # .. format & provide its version
            return f"v{package.version}"

    # Provide fallback
    return "dev"  # pragma: no cover


def file2hash(path: str) -> str:
    """
    :return: str
    """

    file = config.app_dir / "assets" / path

    if not file.exists():
        return path

    # Format 'modified' time
    mtime = str(file.stat().st_mtime).replace(".", "")

    # Build filepath
    return path.replace(file.name, f"{file.stem}.{mtime}{file.suffix}")


j2_globals = {
    "current_version": current_version,
    "file2hash": file2hash,
}
