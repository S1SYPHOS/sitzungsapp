"""
This module is part of the 'sitzungsapp' package,
which is released under GPL-3.0-only license.
"""

from fastapi import APIRouter
from fastapi.responses import FileResponse

from ..config import config

# Initialize router
assets = APIRouter()


@assets.get("/static/css/main.{string}.css")
async def styles() -> FileResponse:
    """
    Serves cache-busted CSS file

    :return: fastapi.responses.FileResponse
    """

    return FileResponse(config.app_dir / "assets" / "css" / "main.css")


@assets.get("/static/js/main.{string}.js")
async def scripts() -> FileResponse:
    """
    Serves cache-busted JS file

    :return: fastapi.responses.FileResponse
    """

    return FileResponse(config.app_dir / "assets" / "js" / "main.js")
