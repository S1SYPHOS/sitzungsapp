"""
This module is part of the 'sitzungsapp' package,
which is released under GPL-3.0-only license.
"""

from base64 import b64decode, b64encode
from itertools import groupby
from operator import itemgetter

from PyPDF2.errors import PdfReadError
from fastapi import APIRouter, Depends, HTTPException, Request, Response
from sitzungsdienst import StA
from starlette.templating import _TemplateResponse as Template

from ..config import config
from ..models import FormData, PostData
from ..templating import jinja2
from ..utils import data2hash, human_size

# Initialize router
api = APIRouter()


@api.get("/")
async def index_get(request: Request) -> Template:
    """
    Serves 'index' page (GET method)

    :param request: Request
    :return: starlette.templating._TemplateResponse
    """

    # Redirect 'GET' requests ..
    return jinja2.TemplateResponse("api/form.html", {"request": request})


@api.post("/")
async def index_post(request: Request, form: FormData = Depends()) -> Template:
    """
    Serves 'index' page (POST method)

    :param request: Request
    :param form: FormData Uploaded form data
    :return: starlette.templating._TemplateResponse
    """

    # Fail early if ..
    # (1) .. no files uploaded
    if form.is_empty():
        raise HTTPException(400)

    # (2) .. upload limit exceeded
    if form.total_size > config.max_content_length:
        raise HTTPException(413)

    # Attempt to ..
    try:
        # .. extract data
        dates, express = await StA.run(form.files)

        # .. filter it (if needed)
        dates = dates.filter(form.search_terms)

        # If no results available ..
        if not dates:
            # .. report back
            return jinja2.TemplateResponse(
                "api/form.html",
                {
                    "request": request,
                    "message": "Für die Suche gibt es keine Ergebnisse!",
                },
            )

        # (3) Build 'base64' encoded string
        base64 = str(b64encode(dates.data2ics().serialize().encode("utf-8")), "utf-8")

        # Convert data structure
        # (1) Court dates
        court_dates = [
            {
                "who": date.assigned,
                "date": date.date,
                "when": date.when,
                "what": date.what,
                "where": date.where,
            }
            for date in dates
        ]

        # (2) Express service dates
        express_dates = [
            {
                "who": date.assigned,
                "start": date.start,
                "end": date.end,
            }
            for date in express
        ]

        # Store information about assignments like ..
        info = {
            # .. their number
            "count": len(court_dates),
            # .. their unique hash
            "hash": data2hash(court_dates),
            # .. their 'base64' representation
            "base64": base64,
            # .. its estimated file size
            "size": human_size(base64),
            # .. used search terms (if any)
            "query": form.query,
            # .. express assignees (if any)
            "express": express_dates,
        }

        return jinja2.TemplateResponse(
            "api/data.html",
            {
                "request": request,
                "info": info,
                "data": {
                    day: list(items)
                    for day, items in groupby(court_dates, key=itemgetter("date"))
                },
            },
        )

    # If file(s) could not be read ..
    except PdfReadError as error:
        # .. report 'Unsupported Media Type' error
        raise HTTPException(415) from error

    # If something goes south ..
    except Exception as error:  # pragma: no cover
        # .. report 'Not Found' error
        raise HTTPException(404) from error

    return jinja2.TemplateResponse("api/form.html", {"request": request})


@api.post("/download/{data_hash}")
async def download(data_hash: str, data: PostData = Depends()) -> Response:
    """
    Downloads ICS data file

    :param data_hash: str Cryptographic digest over data
    :param data: PostData Base64-encoded ICS data
    :return: fastapi.responses.Response
    """

    # If response is empty ..
    if data.is_empty():
        # .. report 'Not Found' error
        raise HTTPException(404)

    # Build filename
    filename = f'attachment; filename="termine-{data_hash}.ics"'

    # Create calendar file response from bytes-like object
    return Response(
        b64decode(data.base64),
        media_type="text/calendar",
        headers={"content-disposition": filename},
    )
