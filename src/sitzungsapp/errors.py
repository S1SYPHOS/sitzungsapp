"""
This module is part of the 'sitzungsapp' package,
which is released under GPL-3.0-only license.
"""

from fastapi import HTTPException, Request

from .config import config
from .templating import jinja2
from .utils import human_size


async def error(
    request: Request, exception: HTTPException
) -> jinja2.TemplateResponse:  # pragma: no cover
    """
    Handles HTTP errors

    :param request: fastapi.requests.Request
    :param exception: fastapi.exceptions.HTTPException
    :return: starlette.templating._TemplateResponse
    """

    # Determine upload limit
    max_size = human_size(config.max_content_length)

    http_codes = {
        # Bad Request
        400: "Keine PDF-Datei(en) ausgewählt!",
        # Payload Too Large
        413: f"Die maximale Dateigröße von {max_size} wurde überschritten!",
        # Unsupported Media Type
        415: "Datei(en) mit ungültigem Dateiformat ausgewählt!",
    }

    message = http_codes.get(
        exception.status_code,
        "Ein Fehler ist aufgetreten, bitte versuchen Sie es später noch einmal!",
    )

    return jinja2.TemplateResponse(
        "api/form.html",
        {"request": request, "message": message, "error": True},
        exception.status_code,
    )


http_error = [HTTPException, error]
