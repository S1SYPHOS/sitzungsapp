"""
This module is part of the 'sitzungsapp' package,
which is released under GPL-3.0-only license.
"""

from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates

from .config import config
from .jinja2.filters import j2_filters
from .jinja2.globals import j2_globals

# Define directories
# (1) Static files
static = ["/static", StaticFiles(directory=config.app_dir / "assets"), "static"]

# (2) Template files
jinja2 = Jinja2Templates(directory=config.app_dir / "templates")

# Extend environment
# (1) Template filters
jinja2.env.filters.update(j2_filters)

# (2) Global functions
jinja2.env.globals.update(j2_globals)
