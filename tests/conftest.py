"""
This module is part of the 'sitzungsapp' package,
which is released under GPL-3.0-only license.
"""

from fastapi.testclient import TestClient
from pytest import fixture

from sitzungsapp import create_app


@fixture(scope="module")
def test():
    """
    Provides client for testing
    """

    # Create application
    app = create_app()

    # Prepare client for testing
    client = TestClient(app)

    yield client
