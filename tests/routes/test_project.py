"""
This module is part of the 'sitzungsapp' package,
which is released under GPL-3.0-only license.
"""

from fastapi.testclient import TestClient


def test_project(test: TestClient):
    """
    Tests 'project' page
    """

    # Run function
    response = test.get("/ueber-das-projekt/")

    # Assert result
    assert response.status_code == 200
