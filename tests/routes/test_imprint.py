"""
This module is part of the 'sitzungsapp' package,
which is released under GPL-3.0-only license.
"""

from fastapi.testclient import TestClient


def test_imprint(test: TestClient):
    """
    Tests 'imprint' page
    """

    # Run function
    response = test.get("/rechtliche-angaben/")

    # Assert result
    assert response.status_code == 200
