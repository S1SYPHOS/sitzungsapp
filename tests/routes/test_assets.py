"""
This module is part of the 'sitzungsapp' package,
which is released under GPL-3.0-only license.
"""

from fastapi.testclient import TestClient


def test_styles(test: TestClient):
    """
    Tests cache-busting styles
    """

    # Run function
    response = test.get("/static/css/main.12345.css")

    # Assert result
    assert response.status_code == 200


def test_scripts(test: TestClient):
    """
    Tests cache-busting scripts
    """

    # Run function
    response = test.get("/static/js/main.12345.js")

    # Assert result
    assert response.status_code == 200
