"""
This module is part of the 'sitzungsapp' package,
which is released under GPL-3.0-only license.
"""

import pathlib

from sitzungsapp.utils import data2hash, human_size


def test_data2hash():
    """
    Tests 'data2hash'
    """

    # Assert result #1
    assert (
        "99fecba92d36c730851d5a1967a68c0be7e0d0e79145ac0049fc6680279703e2e"
        + "cd4325c9fb469acf7d05b758f97e7b24f2482991545f987e21773d94e05afdd"
        == data2hash("string1")
    )

    # Assert result #2
    assert (
        "6d13aefb9edf3ea9075d2a0b77df5139cad99ebb3115e413570cc9409405eb205"
        + "f6c5fe0a88b22c5bd2e77afd7bb94bfc4869eb39c2daaaca3a63743fa7c15eb"
        == data2hash(["string1", "string2"])
    )

    # Assert result #3
    assert (
        "0032a3754a5e48f34b9cee4948aa6f5ce1a9ac598f0812349e09db8511e4c6e2b"
        + "8daaac2a966fbf5839dcf98ab83a93be38419fd7d9538dfa33c3e88fac5923e"
        == data2hash({"string1": "string2"})
    )


def test_human_size():
    """
    Tests 'human_size'
    """

    # Setup
    # (1) Directories
    root = pathlib.Path(__file__).parent.parent
    path = root / "src" / "sitzungsapp" / "app" / "assets"

    # (2) Target files
    pubkey = path / "pubkey.asc"
    favicon = path / "favicon.ico"

    # Assert results
    # (1) Files (both string & path-like objects)
    assert human_size(pubkey) == human_size(str(pubkey)) == "660 B"
    assert human_size(favicon) == human_size(str(favicon)) == "4.19 KB"

    # (2) Numbers (both integers & floats)
    assert human_size(1024 * 1024) == "1 MB"
    assert human_size(1024 * 1024 * 1.0) == "1 MB"

    # (3) Bytes & strings (with support for 'base64')
    assert human_size(b"Nice try :D") == "11 B"
    assert human_size("Nice try :D") == "11 B"
    assert human_size("TmljZSB0cnkgOkQ=") == "11 B"
