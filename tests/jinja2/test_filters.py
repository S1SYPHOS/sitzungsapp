"""
This module is part of the 'sitzungsapp' package,
which is released under GPL-3.0-only license.
"""

from sitzungsapp.jinja2.filters import reverse_datetime


def test_reverse_datetime():
    """
    Tests 'reverse_datetime'
    """

    assert reverse_datetime("1990-10-03") == "03.10.1990"
