"""
This module is part of the 'sitzungsapp' package,
which is released under GPL-3.0-only license.
"""

import re

from sitzungsapp.jinja2.globals import current_version, file2hash


def test_current_version():
    """
    Tests 'current_version'
    """

    # Assert result
    assert isinstance(current_version(), str)


def test_file2hash():
    """
    Tests 'file2hash'
    """

    # Assert result
    assert re.match(r"pubkey\.\d{17}\.asc", file2hash("pubkey.asc"))


def test_file2hash_invalid():
    """
    Tests 'file2hash' (invalid)
    """

    # Assert result
    assert file2hash("invalid") == "invalid"
